import java.util.*;

public class Gravitacija {
    public static void main(String [ ] args){
        
        
      Scanner input = new Scanner(System.in);
      double v = input.nextDouble();
        
      System.out.println("OIS JE ZAKON!");
      izpis(v,Izracun(v));

    }
    
    private static double Izracun (double v) { 
        
        double a = 0;
        
        double C = Math.pow(10, -11); 
        C = C*6.674;
        
        double M = Math.pow(10, 24);
        M = M*5.972;
        
        double r =  Math.pow(10, 6);
        r = r*6.371;
        
        a = (C*M)/((r+v)*(r+v));
        
        return a;

    }
    
    private static void izpis (double v,double a){
        System.out.println(v+" "+a);


    }
}